/* 
 * PaperCalculator.java  1.0  2016-11-03
 * 
 * Copyright (c) Tallinna Paber
 */

package paperCount;

import java.io.IOException;
import java.util.Scanner;


/**
 * Counts the prices for letterpress printing on handmade paper.
 *
 * The calculator counts the final price and price per piece with and without
 * VAT. The calculator takes in account the amount of the order and the number
 * of printing cycles. It generates a .txt file with the price offer in English or Estonian.
 * and saves it on the user's Desktop.
 *
 * @author Eve Ormisson
 * @version 1.0 03 November 2016
 *
 */

class PaperCalculator {

    public static void main(String[] args) {
    	
        Scanner input = new Scanner(System.in);
        PriceChart priceChart = new PriceChart();

        int amount; // Order amount
        int type; // Hand made paper or seed paper
        int format; // 7 paper formats available
        int cycle; // Number of printing cycles
        int language; // Price offer language, EST or ENG

        // Asks the amount from the user and checks if the entry is valid.
        while (true) {
            System.out.println("Enter the amount of cards for your order: ");
            amount = tryParseInt(input.next());
            if (amount > 0) {
                priceChart.setAmount(amount);
                break;
            }
            System.out.println("This entry is not valid, please try again.");
        }

        // Asks the paper type from the user, checks if the entry is valid
        while (true) {
            System.out.println("Enter the paper type.");
            System.out.println("For hand made paper insert 0, for seed paper insert 1");
            type = tryParseInt(input.next());
            if (type == 0 || type == 1) {
                priceChart.setType(type);
                break;
            }
            System.out.println("This entry is not valid, please try again.");
        }

        // Asks the format from the user, checks if the entry is valid
        while (true) {
            System.out.println("Choose the paper format from the 7 available formats.");
            System.out.println("For this insert the number infront of the format.");
            System.out.println("Format 1: Business card (9x5 cm)");
            System.out.println("Format 2: 10x7 cm");
            System.out.println("Format 3: Postcard (A6)");
            System.out.println("Format 4: Bookmark (21x7 cm)");
            System.out.println("Format 5: A5");
            System.out.println("Format 6: A4");
            System.out.println("Format 7: A3");
            format = tryParseInt(input.next());
            if (format > 0 && format < 8) {
                priceChart.setFormat(format);
                break;
            }
            System.out.println("This entry is not valid, please try again.");
        }

        // Asks the number of printing cycles from user, checks if the entry is valid
        while (true) {
            System.out.println("Enter the number of printing cycles of the design: ");
            cycle = tryParseInt(input.next());
            if (cycle > 0) {
                priceChart.setCycle(cycle);
                break;
            }

            System.out.println("This entry is not valid, please try again.");
        }

        // Asks the user to choose the language of the price offer, check if the entry is valid
        while (true) {
            System.out.println("For the price offer file in English, type 0. ");
            System.out.println("For the price offer file in Estonian, type 1. ");
            language = tryParseInt(input.next());
            if (language == 0 || language == 1) {
                priceChart.setLanguage(language);
                break;
            }

            System.out.println("This entry is not valid, please try again.");
        }

        input.close();

        // Writes an invoice in ENG or EST to Desktop
        try {
            priceChart.generateInvoice();
        } catch (IOException e) {
            System.err.println("Could not write the file, sorry " + e.getMessage());
        }
        

        System.out.println("A text file with the price offer has ");
        System.out.println("been created on your Desktop.");
    }

    // author Erki Männiste
    // 19.11.2016 22:00
    // Checks if the user input can be changed into an integer.

    private static int tryParseInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}