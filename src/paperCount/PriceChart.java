package paperCount;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

// Calculates prices and generates the price offer according to the user input.
public class PriceChart {

	private int format;
	private int type;
	private int amount;
	private int cycle;
	private int language;

	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

	public int getFormat() {
		return format;
	}

	public void setFormat(int format) {
		this.format = format;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	int minOrder = 45; // Minimum order price

	/**
	 * Calculates total price based on price per one card, chosen format and
	 * paper type.
	 * 
	 * @return total price for cards.
	 */
	private double countPrice() {
		double total = 0;
		switch (format) {
		case 1:
			if (type != 1) {
				if (amount < 100) {
					total = amount * 1.1;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 0.8;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 0.7;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 0.6;
				} else if (amount >= 500) {
					total = amount * 0.5;
				}
				break;
			} else {
				if (amount < 100) {
					total = amount * 1.2;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 0.9;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 0.8;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 0.7;
				} else if (amount >= 500) {
					total = amount * 0.6;
				}
				break;
			}
		case 2:
			if (type != 1) {
				if (amount < 100) {
					total = amount * 1.2;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 0.9;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 0.8;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 0.7;
				} else if (amount >= 500) {
					total = amount * 0.6;
				}
				break;
			} else {
				if (amount < 100) {
					total = amount * 1.3;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 1.0;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 0.9;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 0.8;
				} else if (amount >= 500) {
					total = amount * 0.7;
				}
				break;
			}
		case 3:
			if (type != 1) {
				if (amount < 50) {
					total = amount * 2.5;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 2.3;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 2.1;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 2.0;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 1.8;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 1.6;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 1.3;
				} else if (amount >= 1000) {
					total = amount * 1.0;
				}
				break;
			} else {
				if (amount < 50) {
					total = amount * 3.0;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 2.8;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 2.6;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 2.5;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 2.3;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 2.1;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 1.8;
				} else if (amount >= 1000) {
					total = amount * 1.5;
				}
				break;
			}
		case 4:
			if (type != 1) {
				if (amount < 50) {
					total = amount * 2.5;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 2.3;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 2.1;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 2.0;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 1.8;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 1.6;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 1.3;
				} else if (amount >= 1000) {
					total = amount * 1.0;
				}
				break;
			} else {
				if (amount < 50) {
					total = amount * 3.0;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 2.8;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 2.6;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 2.5;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 2.3;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 2.1;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 1.8;
				} else if (amount >= 1000) {
					total = amount * 1.5;
				}
				break;
			}
		case 5:
			if (type != 1) {
				if (amount < 50) {
					total = amount * 3.5;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 3.3;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 3.1;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 3.0;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 2.8;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 2.6;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 2.3;
				} else if (amount >= 1000) {
					total = amount * 2.0;
				}
				break;
			} else {
				if (amount < 50) {
					total = amount * 4.0;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 3.8;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 3.6;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 3.5;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 3.3;
				} else if (amount >= 500 && amount < 700) {
					total = amount * 3.1;
				} else if (amount >= 700 && amount < 1000) {
					total = amount * 2.8;
				} else if (amount >= 1000) {
					total = amount * 2.3;
				}
				break;
			}
		case 6:
			if (type != 1) {
				if (amount < 20) {
					total = amount * 7;
				} else if (amount >= 20 && amount < 50) {
					total = amount * 5.0;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 4.8;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 4.6;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 4.5;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 4.4;
				} else if (amount >= 500 && amount < 1000) {
					total = amount * 4.2;
				} else if (amount >= 1000) {
					total = amount * 3.9;
				}
				break;
			} else {
				if (amount < 20) {
					total = amount * 10;
				} else if (amount >= 20 && amount < 50) {
					total = amount * 8.0;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 7.8;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 7.6;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 7.5;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 7.4;
				} else if (amount >= 500 && amount < 1000) {
					total = amount * 7.2;
				} else if (amount >= 1000) {
					total = amount * 6.9;
				}
				break;
			}
		case 7:
			if (type != 1) {
				if (amount < 20) {
					total = amount * 11.5;
				} else if (amount >= 20 && amount < 50) {
					total = amount * 9.5;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 7.5;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 7.3;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 7.1;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 7.0;
				} else if (amount >= 500 && amount < 1000) {
					total = amount * 6.8;
				} else if (amount >= 1000) {
					total = amount * 6.5;
				}
				break;
			} else {
				if (amount < 20) {
					total = amount * 15;
				} else if (amount >= 20 && amount < 50) {
					total = amount * 13;
				} else if (amount >= 50 && amount < 100) {
					total = amount * 11;
				} else if (amount >= 100 && amount < 200) {
					total = amount * 10.8;
				} else if (amount >= 200 && amount < 300) {
					total = amount * 10.6;
				} else if (amount >= 300 && amount < 500) {
					total = amount * 10.5;
				} else if (amount >= 500 && amount < 1000) {
					total = amount * 10.3;
				} else if (amount >= 1000) {
					total = amount * 10;
				}
				break;
			}
		}
		return total;
	}

	/**
	 * Adds extra 30 % of the total price for every next printing cycle. 
	 * Every new color is also counted as a new print cycle.
	 * 
	 * @return the final price for the order.
	 */
	private double calculatePriceWithCycles() {
		double counted = countPrice();
		if (cycle > 1) {
			double percent;
			percent = counted * 0.3;
			counted = counted + (cycle - 1) * percent;
		}
		if (minOrder > counted) {
			return minOrder;
		}
		return counted;
	}

	/**
	 * Calculates VAT from the price. VAT is already added to the prices in
	 * countPrice method. This method calculates the VAT part form this final
	 * price.
	 * 
	 * @param counted
	 * @return VAT calculated from the total price.
	 */
	private double getVat(double counted) {
		double vat = 0.2;
		return counted * vat;
	}

	/**
	 * Generates text for the price offer in ENG or EST.
	 * 
	 * @throws IOException
	 */
	public void generateInvoice() throws IOException {

		double price = calculatePriceWithCycles();
		double vatCounted = getVat(price);
		double withoutVat = price - vatCounted;
		double oneCard = price / amount;
		String content;
		String fileName = "priceOffer.txt";
		if (language == 0) {
			content = "The total for " + amount + " cards is " + price + " € \nThe price of one card is " + oneCard
					+ " €\nThe prices include paper, " + cycle + " letterpress printing cycles and VAT. \n\n"
					+ "Total without VAT: " + withoutVat + " € \nVAT: " + vatCounted + " €";

		} else {
			content = "Hind " + amount + " kaardi eest on " + price + " € \nÜhe kaardi hind on " + oneCard
					+ " €\nHinnad sisaldavad paberit, " + cycle + " trükitsüklit ja käibemaksu. \n\n"
					+ "Hind ilma käibemaksuta: " + withoutVat + " € \nKäibemaks: " + vatCounted + " €";
			fileName = "hinnapakkumine.txt";

		}

		// Finds path to user Desktop, writes file
		Path userHomeFolder = Paths.get(System.getProperty("user.home"), "Desktop", fileName);
		// Path userHomeFolder = Paths.get(System.getProperty("user.home"),
		// fileName); // works for Linux?
		Files.write(userHomeFolder, content.getBytes());

	}
}